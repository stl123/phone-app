package com.stl.sachithw.jk_display;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * Created by SachithW on 8/31/2016.
 */
public class Calibrate extends Activity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calibration);

        Button B_1 = (Button) findViewById(R.id.button1);
        Button B_2 = (Button) findViewById(R.id.button2);
        Button B_3 = (Button) findViewById(R.id.button3);
        Button B_4 = (Button) findViewById(R.id.button4);

        B_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_1");

                // broadcast a custom intent.
                Intent intent = new Intent();
                intent.setAction("com.stl.sachithw.jk_display.B_1");
                sendBroadcast(intent);
            }
        });
        B_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_2");

                // broadcast a custom intent.
                Intent intent = new Intent();
                intent.setAction("com.stl.sachithw.jk_display.B_2");
                sendBroadcast(intent);
            }
        });
        B_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_3");

                // broadcast a custom intent.
                Intent intent = new Intent();
                intent.setAction("com.stl.sachithw.jk_display.B_3");
                sendBroadcast(intent);
            }
        });
        B_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_4");

                // broadcast a custom intent.
                Intent intent = new Intent();
                intent.setAction("com.stl.sachithw.jk_display.B_4");
                sendBroadcast(intent);

            }
        });

    }

}
