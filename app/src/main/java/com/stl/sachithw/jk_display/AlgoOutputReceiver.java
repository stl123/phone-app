package com.stl.sachithw.jk_display;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import static android.support.v4.app.ActivityCompat.startActivity;

/**
 * Created by SachithW on 8/26/2016.
 */
public class AlgoOutputReceiver extends BroadcastReceiver {

    public static final String TAG = AlgoOutputReceiver.class.getSimpleName();
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        Log.d(AlgoOutputReceiver.TAG, action);
        if (action.equals("com.stl.sachithw.jk_display.B_1")) {
            //start activity
            try {
                Intent i = new Intent();
                i.setClassName("com.stl.sachithw.jk_display","com.stl.sachithw.jk_display.GestureOne");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            } catch ( ActivityNotFoundException e) {
                e.printStackTrace();
                Log.e(TAG,"Activity not found");
            }

            Toast.makeText(context, "Intent B_1", Toast.LENGTH_LONG).show();

        }
        else if (action.equals("com.stl.sachithw.jk_display.B_2")) {
            try {
                //Start your second activity
                Intent intentp = new Intent(context, GestureTwo.class);
                intentp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                context.startActivity(intentp);

            } catch ( ActivityNotFoundException e) {
                e.printStackTrace();
            }

            Toast.makeText(context, "Intent B_2", Toast.LENGTH_LONG).show();
        }
        else if (action.equals("com.stl.sachithw.jk_display.B_3")) {
                        //start activity
            try {
                Intent i = new Intent();
                i.setClassName("com.stl.sachithw.jk_display","com.stl.sachithw.jk_display.GestureThree");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            } catch ( ActivityNotFoundException e) {
                e.printStackTrace();
                Log.e(TAG,"Activity not found");
            }
            Toast.makeText(context, "Intent B_3", Toast.LENGTH_LONG).show();
        }
        else if (action.equals("com.stl.sachithw.jk_display.B_4")) {
            //start activity
            try {
                Intent i = new Intent();
                i.setClassName("com.stl.sachithw.jk_display","com.stl.sachithw.jk_display.GestureFour");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            } catch ( ActivityNotFoundException e) {
                e.printStackTrace();
                Log.e(TAG,"Activity not found");
            }
            Toast.makeText(context, "Intent B_4", Toast.LENGTH_LONG).show();

        }
    }
}
