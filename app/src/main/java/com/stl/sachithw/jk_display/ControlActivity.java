package com.stl.sachithw.jk_display;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;

/**
 * Created by SachithW on 8/31/2016.
 */
public class ControlActivity extends Activity {

    public static final String TAG = MainActivity.class.getSimpleName();
    int count=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.controlpage);

                Button B_1 = (Button) findViewById(R.id.button1);
        Button B_2 = (Button) findViewById(R.id.button2);
        Button B_3 = (Button) findViewById(R.id.button3);
        Button B_4 = (Button) findViewById(R.id.button4);

        B_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_1");

                Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.music");
                startActivity(intent);
            }
        });
        B_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_2");

                // broadcast a custom intent.
//                Intent intent = new Intent();
//                intent.setAction("com.stl.sachithw.jk_display.B_2");
//                sendBroadcast(intent);

                File file = new File(Environment.getDataDirectory(), "test.jpg");
                Uri outputFileUri = Uri.fromFile(file);

                // start camara app
                Intent takePhoto = new Intent("android.media.action.IMAGE_CAPTURE");
                takePhoto.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                startActivityForResult(takePhoto, 1);

            }
        });
        B_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_3");

//                // broadcast a custom intent.
//                Intent intent = new Intent();
//                intent.setAction("com.stl.sachithw.jk_display.B_3");
//                sendBroadcast(intent);

                // take vidio
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                startActivityForResult(intent,0);
            }
        });
        B_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_4");

                // broadcast a custom intent.
//                Intent intent = new Intent();
//                intent.setAction("com.stl.sachithw.jk_display.B_4");
//                sendBroadcast(intent);

                // open the contacts
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                startActivityForResult(intent, 1);

            }
        });



    }

}
