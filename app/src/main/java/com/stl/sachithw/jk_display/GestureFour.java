package com.stl.sachithw.jk_display;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by SachithW on 8/26/2016.
 */
public class GestureFour extends Activity {

    ListView lv;
    Context context;
    ArrayList gestureName;

    public static int [] gestureImages={R.drawable.ic_number_4_in_red,R.drawable.ic_bens_number1_md,R.drawable.ic_number_2,R.drawable.ic_number_3_coner_num_hi};
    public static String [] gestureNameList={"Gesture 4","Gesture 1","Gesture 2","Gesture 3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ges4);

        context=this;

        lv=(ListView) findViewById(R.id.listView);
        lv.setAdapter(new CustomAdapter(context, gestureNameList, gestureImages));
    }
}
