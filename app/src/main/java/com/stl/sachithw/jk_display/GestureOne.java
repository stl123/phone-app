package com.stl.sachithw.jk_display;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by SachithW on 8/26/2016.
 */
public class GestureOne extends Activity {

//    private static GestureOne instance = null;
//
//    private GestureOne(){}
//
//    public static GestureOne getInstance(){
//        if(instance == null) {
//            instance = new GestureOne();
//        }
//        return instance;
//    }

    ListView lv;
    Context context;
    ArrayList gestureName;

    public static int [] gestureImages={R.drawable.ic_bens_number1_md,R.drawable.ic_number_2,R.drawable.ic_number_3_coner_num_hi,R.drawable.ic_number_4_in_red};
    public static String [] gestureNameList={"Gesture 1","Gesture 2","Gesture 3","Gesture 4"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ges1);

        context=this;

        lv=(ListView) findViewById(R.id.listView);
        lv.setAdapter(new CustomAdapter(this, gestureNameList, gestureImages));
    }


}
