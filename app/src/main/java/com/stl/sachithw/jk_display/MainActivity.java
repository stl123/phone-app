package com.stl.sachithw.jk_display;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        final Context context = this;

        Button B_calibrate = (Button) findViewById(R.id.C_button);
        Button B_control = (Button) findViewById(R.id.contrl_button);


        B_control.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "B_1");
                //start activity
                try {
                    Intent i = new Intent();
                    i.setClassName("com.stl.sachithw.jk_display","com.stl.sachithw.jk_display.ControlActivity");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                } catch ( ActivityNotFoundException e) {
                    e.printStackTrace();
                    Log.e(TAG,"Activity not found");
                }

                Toast.makeText(context, "Control", Toast.LENGTH_LONG).show();
            }
        });
        B_calibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Calibrate Button");

                //start activity
                try {
                    Intent i = new Intent();
                    i.setClassName("com.stl.sachithw.jk_display","com.stl.sachithw.jk_display.Calibrate");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                } catch ( ActivityNotFoundException e) {
                    e.printStackTrace();
                    Log.e(TAG,"Activity not found");
                }
            }
        });


    }

}
